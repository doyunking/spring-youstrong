<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
    html,body {	
	 height:100%;
	 background-color: black;
    }
    #header, #nav, #section, #article, #aside_left,#aside_right {
      display:block;
      width:100%;
      margin:4px;
      padding:4px;
      background-color:#ddd;
      text-align:center;
    }
    #header{
   		height : 10%;
    	 background-color:black;
    }
    #nav{
    	background-color : black;
    	height : 5%;
    }
    #section {
	  	display : flex;  
	    width:100%;
	    height:100%;
	    background-color : black;
    }
    #article {
      height : 100%;
      background-color: dargray;
      flex : 5;
    }
    #aside_left {
 		display : block;
		float:left;
		width:10%;
		height:100%;
		background-color:black;
		flex : 1;
		padding:5px 5px 5px 5px;
		
		overflow: hidden;
		justify-content: center;
    }
     #aside_right {
     	display : block;
		float:right;
		width:10%;
		height:100%;
		background-color:black;
		flex : 1;
		padding:5px 5px 5px 5px;
		
		overflow: hidden;
		justify-content: center;
    }
    .login_status{
    	float : right;
    }
    .logo{
    	float : center;
    }
    .myButton {
    	float : center;
		box-shadow:inset 0px 1px 0px 0px #ffffff;
		background:linear-gradient(to bottom, #f9f9f9 5%, #e9e9e9 100%);
		background-color:#f9f9f9;
		border-radius:6px;
		border:1px solid #dcdcdc;
		display:inline-block;
		cursor:pointer;
		color:#666666;
		font-family:Arial;
		font-size:15px;
		font-weight:bold;
		padding:6px 24px;
		text-decoration:none;
		text-shadow:0px 1px 0px #ffffff;
	}
	.myButton:hover {
		background:linear-gradient(to bottom, #e9e9e9 5%, #f9f9f9 100%);
		background-color:#e9e9e9;
	}
	.myButton:active {
		position:relative;
		top:1px;
	}
	
</style>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script>
 
	function movepage(url){
	
	    // ajax option
	
	    var ajaxOption = {
	
	            url : url,
	
	            async : true,
	
	            type : "GET",
	
	            dataType : "html",
	
	            cache : false
	
	    };
	
	    
	
	    $.ajax(ajaxOption).done(function(data){
	
	        // Contents 영역 삭제
	
	        $('#article').children().remove();
	
	        // Contents 영역 교체
	
	        $('#article').html(data);
	
	    });
	
	}
</script>


</head>

<body>
 <div id="header">
	<div class="login_status">
		<a href="#" style="color:white">로그인</a>
		<a href="#" style="color:white">회원가입</a>
	</div>
	<div class="logo">
		<a href="index.jsp"><img src="/static/image/logo.gif"/></a>
	</div>
</div>
<div id="nav">
	<a type="button"  onclick="movepage('/free')" class="myButton">자유게시판</a>
	<a type="button"  onclick="movepage('/share')" class="myButton">정보 공유</a>
	<a type="button"  onclick="movepage('/todayexercise')" class="myButton">오늘의 운동 기록</a>
	<a type="button"  onclick="movepage('/threerecord')" class="myButton">3대 기록</a>
	<a type="button"  onclick="movepage('/recommendgoods')" class="myButton">추천 상품</a>
</div>
<div id="section">
  <div id="aside_left">
  	<div><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGwlOHy78h8p9TG2lzG_8tLlVTRwnWoSC3HXgmKcAifaB-JL90hg&s" width="100%" height="40%"/> </div>
  	<div><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFRUVGB0YFxgXFxcdGBcYGxcYGBoaGBcYHSggHx0lGxgYITEhJSkrLi4uFyEzODMtNygtLisBCgoKDg0OFxAQFysdHyUtLS0tLSstLS0tKy0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0rK//AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBgADBQIBBwj/xABDEAACAQIEAwUFBgUCBQMFAAABAhEAAwQSITEFQVEGEyJhcTKBkaGxFEJSwdHwByNicuGCkjNDU6LxFoPSY3OTo7L/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAAhEQEBAAICAgMAAwAAAAAAAAAAAQIRAyESMRNBUQQiYf/aAAwDAQACEQMRAD8AWuF9msLZxNi9cxS9wHBIuCHkAlfZJDePLOg0mnv+HXaZ7t7F21e13rOe5S4NTpE51PiULbGg8jsSayOyOAsnD27tq2isV3iX0JBBcidwdo0iu8XwKzhbd7iLE9/cv5cMFJASbWV2I5nxO3+lOppJ0tV/xbwt1XtNeyG5esQ5tzkLqSCVnWIKGvjxcczX2LhrpjbIt4y4Vs8Pm81wTmOHZTNoEc86KBziQNQKJ473+GwwxGBw1qyt5VuDCmwlw9yoIZ2MSbgm2XUaANMt4jUHybgPGr2DvpiLD5XQz5MOasOakaEfnBr9I8I7XYfHYRcTabJcUarqTbePEjEDVT15gg7gR8r7FWbXEnbDXMKljELbLrcS3Ft1DAZbtpho3i0dYkDXYS29i+y1/h+IbEWRmsuMmJsg5tBJW7bG5ymfDzUtGoikHT8cWzie9YlrF+0wWBLBwyt3RjQrrcIzTl8YBApGxfaO2cLeweGtXLK3GL+O4rZVEl1tjcFhOkka6Gmjtrdw2JJGHuKVaG8P3GkAOPeQD6mktMDbfDuTaW25Ym25k3QkwC5OxI3UCPIUpHX/AKmQ5e7tlEQKtvUSAoAhv3t76X+1Kg3u8H/MEn1Gk/DLR/dWTkAt5CQFYAnRgcrTPLnPpWTxowwXfLt8KgzqlSieGYcXL1q2dnuKp9CwB+U0RvcA7MI6pdxTtbS5rbt24N64PxAQcq+cHfkIJb7HCUW+9nDWSEteEopZ2VwgZnuk6sTMBhIGo054WI4hrduzFy5cy2wFJIt2SGW2sGVkkCBM66ayGbtBhb2HJVi1t8T3d4ZSwzOqBHtmNZEBgNdzRoHw7te2HvuVuFrTNDDXKAQOoBzCDyHv2q7iPau7dbKRmXkD57Qf3pWPe4Xed7irkJY+IKT4DOYBmYASRpAn51mcRuFVuJuUUoY5lZWfTelHnF+PZAVskF9ZcbL5J1Pn8OtedlLwFi87t/zASTufAT7zSuRA+n7601djh/Ic8xeEbaEIDz91EbnD8SLttwsnwwQQQQcy7g+tKfb2RjbhYEAhCpPMC0gkddQR7qdOF2jnu3GYu1zUkgDXMoAAUACre1var7IFtL/MdkByEnIoiMzDmTG3ONxRXy3E4drbFHUqy7gxppPLyNVURxHGvfuveuEF3MmBA2AAA6AAD3UPRlKlSpQSpUr2KDypUqUEqVKlB7UryvaCV5UqUEqVKlB9S/hpj5RrRPst8nWR/wByN/uozt5bMWtTlBYRyDMF19SFj3UodkXuWMTDAgMMsHSGUhxPwI99OnbHEzhLzAAkLIkezBHiXoQJ+nOtSqo4hwuza4J3lty/2u9bS6Z8OW1ffwgcgDM9afeJ3Bi7PcW27nGWibmHndbtrKAQdijq4XzW6dJBA+S9m8d3/BsXgyfFYuJeTyRyA0ejqf8A8lM+B7T2Fu4e8zXUuXUXM+rW20CG0VnwgMFaQNCfM1A8dlu1qYqwbrDu7loHv0O6OoM6Hl0/UGsj+H/EnucPs3Q2W6e8OvMtcdiD1/xNLfabG4O1dxb275XE4iw9q5bAJt5yNGYqvhfyJ+8SYmax+z/EkSxYRSBewzkspfJNtvE5B9lgQF0PnsabFPaHD93jPtCjJ3rlL6jQK7+HPG2pIJ8wDzpX4ncuJfYW2ID+IqxlZbfQ7CZEU7drm7y13rRLTau5QQD+BhPlpMnWINJHaWW7u5zYax1IDQPfmFRatt4sEFiQsRI6sBGnu0novWsPFXs7luv0ogYG62+n9xj4jf41RewjLrEj8S6j40RTVuDv5LiXPwMrfAg1TNMOB4XYs2kv4zM5ujNaw6NlYpsLt191QnZRq28xRD3/AAs4Qt7F3L1zVMIcyj7pdzNs+cZWb1CHkKcf4icTwqYeMSucsZtKph84+8rfdA0k+fMxSV2f7SXLeGN7Drbs2wR4VRZgNkJYldYjz9a18dxfDYsZOIYZGMQt+0CrqJnWNYnUwY8jV3qLoj2+1LLmW3bi4dWdnDQYAkQokwKzr7CPVZ940M+utd8Y4SMNcY23720dVbQmOU8jr5f5zcVioVRP3Y+NRWWzU+/w8shrKgj2sS4PoLFqPma+f0//AMPbuW0vliLh/wD02R+VWIdOL4VUt+Ea51HumT9K+S9rrhfH3xqWNzKo5kABVAHmAI9a+s8RxAZVHVx9DSx2s7XCy/cqq3XWD49Utn2hHPNz0I3pfa/T5s6EEqQQQSCDoQQYII6zXld4i8XZnYyzMWJ8yZPzNcVGUovh3Drl5oQaDdj7K+p6+Qk1xw7Bm7cW2NJ3PQcz++cU9YW0qKEQZUXSObeZP586lrWOO3HCuz2GtCWt/aHH/UZhbH/t2yP+5jPlTFYvWlEDB4MDoLC//KszOAIkDpXa4gyBmT8zt59ZqOmpBWJ4bgLwi5g7dsn79jwEecLE/OlHj3Ys2wbmGfvk/CdLg+k+kA+tM1q4xPKNZj120PL0opdifKm08ZXyKpTP2r4VDd4o1bXT73Wf6h86WK052aSpUqURKle1KDyvalSg1hjmDi4zEkMGJ66yafb17vEKHZlI8oI/zXzem/gl3PZQ6yoyHzjr7qNMjsTYvJiR/LY2nDWbxMKuRtCczQPCwVtPw0WMR3Vw2wwYKxyOjCJnxZTqpBgGDzB2rP7QYnLcKuzNEFV1gCPPSu+F8OF+wWtE94rHMhiOqlDpGnXmDRGjxK3ZIvYm6bhVmnu0hTLmDLTrB16etZ68LXvgpuHxKDaeNCIJAPn+hoZ7xe2bTbll+E70bfxNq6oVHAdfZmQZHSfSij7VwmzctnYofusJdNBMjeVH71rAa9nw55lCfrmnT+5vhTAmPm0WOkKc3qBrSzwldGU/eUH4afRiaJXPCXbvBE+Yn9afzwdGt94DlaPbAEg9LinQj166RvXzdCVYEEqwO40g+VOHB+0BurlaBdUa8luL1jr9PSRUqxVi+A2GuKwGQqwNxN0YbnL5Ej5kVjPZfFMz54JPhLloKjQLIB1URptryrfuXAWAGxDR5GDI+XyrK4Iwi5bYSsgxsRoIKnkfOkK3MEot2O4BJUIw15kksTHqTQGC4mQiggsUOVjI0A+8flXOd0YLrcU+y4jMP7x5daAYBbrBvZYCZ26DX4a1RrNeknqDMSD4eR06/uRrWB2g4f3bB1/4b7f0np6cx7+lauYA5ZjTwCeXPMdBGhiZq6xkuLkuLmTQ5Q0GBqQGGxB+tQJ1OHZG4wsQiF3N1sqjSTltgyToNNfdV2G4Zwq5uMZa/ta23/8AU0ecJhrGW1hbly8rSf5qgNnOUFdABEBdfM1SRpcOxvfKukMl0BxMwRmUwedfPuLYC+2KdWtPnu3HKBgRnBYsCpaARl5jSK+g4BEQoiBQCR7IgE+KTHnWB2i7a3ld7Fg5UUlWbeW2JUEwIOkwTpQI4Ne17FSKMtzswF8WviYhROwGpmf3sKZcFi0ZSytmAMenl5Ur8DugW7qnmJ+X/miOzbAd4CYkL8if1qVvGmUqGOuoHLX8vrVyW10EaAz79f1rNsX2LrbQZixhfInnPIefIUz3eCMR/Lvh2G4y5TI9ojWIA61G9sy1hRpuY8/T9KJzQRXWJU20hiAZhgojXqT56kRFZ7YjkPCPKibE4zDqw7skDNqp08LDY/P4E1834rh8lwjYHUeXUfGaZ8dxp0v90oTICBJBzEkCTM/1bRyrO7XYZluKWiXXOY5SSCPiDVjOXZeqV7XlVhK9rypQe1K8qUBweda3eyt3xOnUAj3f+aXrG1aHB7+S8p66fGiwd2p4a9y5ba2hYlSDHKDIknQbmiuzPC3w7m47AyIKCYOsglvLXYczrW1duhQWYgADUnYVgY7tENrSlz1Og+G5+VALx9Ct1nAAzHOAJgE7x056eRrOTE4cwz2rgeQWyOuRjMzBWV9AfhR12615FuTMAq68lMyGX5T5Gsc2taDR4hxNXGVFyoxljrr855CfpV/2pSEUgSAFzCdiI5jpWStvQr7x60dwxxBBHOfy/KqAsTb8TGJ5n86o70qwddCNR5fvWiMePGRMD9dRQpcgwdYqDXu8RgrcG0gkdOvy0rvFIVdssw66RzI0I/2xWNdOgitHCXs9vL963qPQbfDb0obbeFBVGziD3cKZO+mvLWJrKdmUwwZkYEeI5iZETG4ru3bLEugJJ1Oo0/Fmnl+vxvxJzIRI20gg6jUQR5iigbGKZQFOo5Hn8d6MuXcy6ctarsgOgJ3jf/NRBEz035GTQcJxJFYghp8oj60w8Kcd0+J9bVqfxuBnb/TbB97rSTdlnMAkkwANydgB6004l8j28Gmq2QVZgd7xOa83pm8I8rYogvjOOZLSsjQwHhI5HKP1pJj48zTRxggWiMoM7Ez4dV2pdyUKHIqCrjbrzJ0oNLs4yZyHWSYIPSD4iRz3HwphvYWzbZgECtzylzm5g+MnLodh1qrh38PMdlF+53WGQalr9zKQJgyqhjqJ0MU3YTheEw5F7F3TiMo/lWhbZTcIOneSSMuxAJ1B1ECDm2b1tuY3W9MTheAyZcTd/l24JRfvXRBBM/dt66vz2UE6g+zxZf8Aiq6o0EBEUqzayA3IJ/TruSSSdMrtL2iuYu8LlweFdFtqICqAYI6QY1O+vkBRBWAVk88pOUaT7UH4xr5VSX9GfbLjEm7DZjJA2EaCI2gaD869e3zGo+YoNMUPFIK5GCkGDqY2I33otXg6Gopf4lhX+0E5CcxUiAYIyrz9xn0qvtPfzlD/AECfWSfzpoOPZZykqACTqcpOnLYwJ98Ul8Yxpu3C58gB0AED5CtRmswiuSK6ajuE8JN8Oc4TLESCQSZ0JGoiOh3oyzTUrVv9nsQuyrc/+2wJ/wBph/8AtrOuWGVgrqUJ5OCp+B1oiupRRwR5MpHrUoJZYk686u2IPQzXiW2JLx4RrJ0E/h1+8eg157VoW8E72muqJA38j50U3nDd7YP9aEfEUiGyRuCCNwdwf1r6P2LdbmGTqJHw0pP7SZbWJvK7a5ydd4MMPkRSNZLuztq0WPe37dtYE5mUa76yZ2Ma1mcWw1tL1zu7lt7cypW4p0OsadDNDrj1nRZ9QKrxmLY7AD0FVkGcSegojAXZaOZ+fpVIsO28++juG8ODOoLKms5iYGmvP/FQV4+1JHn+/wA6BccjuPnTNxvh72gGKRDSCRKt6HYj0rG4ljnbQ27Vv+y2FJHmTr8KoAK7L+5q+xKGaGozBKx0iRURqYjBlfEAGVokQI+fOu0QkeFZjWANvUbij8cYsP8A2gfMCl23xN01lTGxMae8Hf1pVXW8DecBURmTUaDwgyfabYadTyqYgZF7pWD5faZfZJ5hTzA2nnrXj8QuXvCWOswAzR79Y86IXCwAOgoPOB28jNfP/JAKed5pFvTyhn/9utPgGDmX1J2HXqT+/Oub+DfNZwdtZcmXA3724AYI6IgUHp4/Om3thdweFWzhcIWa6ifzmMxJ1Gb/AOpJJjYAwRtAKnHuScxv8Z/Ssc2qPvEscx1JqspQBNaq/hRyX7Td2buV1IQAksZ0AA1JmCBziKKweDe662raF3cwqqNSfpHmdBTpwrFYbhRJhL2LKkG7ulox7NoR4uYL6SdBpMtBn44xs91exdwG7MrYRQWAIicxYBTMHbqNeSJ2puO1g4hnYnONG3ZczL56iFJ+lV4fid3EHE3bhk+CMwB1LMZkjeF5RGwihe0OHY2VJYsFafLxaGB6kfGvPqTOSPXLbx21TgcWlwa+11G/+pefr9aLbDTHstG0NBjoAYPIfClsW41FEpjLg5z616NPLK2ThgAJEANn1O7dSTvXfeAanbnyHxNYrcSflA91BYm+7e0SR8vhU0uxPGuL5/Ans8/PyHl9axcxq1krgpVZcTTL2Wxtq3bYXGXVpg6ECI0OxmNtdqWyteGgYh2htsTKMonTY6cp21ijbHFEYZRdBB+623+1tD8KTya5mgc/sNr/AKFo/wCgflpUpSXEuBAY1KDQfH2hqFzH0H1q7A4g5YBMGCROhIkSR7z8aP4h/D/F2ZJVboHtdy2ePUQCfcDWTbTIQBt+x+VA1dmePPhCwCJctOZKNIynqjD2SYqdpsdYxIvObQth0U22lWYXbcyO8yg5SDly8tzvpj4cTC9SB8TFOXbvBKMNaKqALLAARoFIj6haD53Ywo5fE/kOdELYA1+tdm5XmpjSqPSorhtNudECzVi2PKoBsPjriAqpOQ+0u6t6qdDQN+zmMkE9OQHkANBW4uGHSre6UcqDDscLG5mPOr7VwBwpIROoGp8s0HL6wa3OHcKvYp+7sJmPNjoq/wBzflX0Ps//AAvwqwcRmvvz1ZbY9ApBPvNNtTG+wPYjsfbe0mKxKLdS4xyISGthFn+Y0aNMGAdIj3a1zjKXHOHs4JQrAqrIFFwLqMygKQgkQJ56emTxHtkiWkwmGUJbFrL6ArGVR6GsrhPGfsoa4sG5cJjoi6fMkT7h1NcOTLt6ePCalEcR4Rg8TeYG0cLlYILoyi7duKP5iXFJCDLuzRy03rDxfBksFry3BfsWjLNBXxiMqMrcmYqMw0Ou1XcW43dusWuEGfIb+XTaq27YX7dj7NZCLbOrsVUlid5J3/TSmOdM+Oa/0H2b4z9la7iYL4u4MuHBEgNcJ7y8fMDRRzznlQFxEygB+8uuxa60zHMieZJOp8jTZwbhv2TB3uJsM124pGGJUlbQueHvDpEgE68gI+9Q/YHsdbuYdbt5nIfZE3YbS78p6DXzrrc5Jt5phlbqFpLDO2VEZ2/Cilj8BWphOxuPukBcOVn7zsqgebayAPT0Br7BwrhJsrFqzbsICDJEyOZaNztuax+Nccv4y6cFgY0EvcJhVWYkkbSdBGp1jYkTHK3v01lhMet7pU419m4bh/s+Hu58U+l65oCw5205qswSAZManoi4gmZeQT+LT619z4HwnCYVMrWz3u1y6ykl2H9Y2XeFGg+JrrjnHUt2zFlYOzLqB66SPfUvLpqcNr4/wFA1jEHkCpkdRMD4mjcfam1HW2fiACPpTxxLhjXLKl3VhdAbKOnIkik3i47oZW6bn061wuflk9Uw8cNb2VQlQ26JFuuSlet88GVrhrdHlKrNqgBa1VZtUebVcFKABrVUMlaLrVLWqABlrgijHt1SyUFMVKsyV5QfdOH8asi0cRhj3oQg3bKHMyp950XeV3Kc1mIjVe/iZgsNdtWsfhgsuwDsnsurAw5H4gwAnfxa7V874ZxO5YdbiNBUyCDuNJB+A9CJptxuND2nIEW8SPGkQqYiZS6saAOyhWA0DQfvVNLvZds3CCCORB+GtfUeOYY3cI5KyrpmRhqrEeIQdtxXymw8imHs5jsWCbWFvMmYHMCQbccyyMGWddwJqkrPfBMjZXVlYAGGBB1EjQ+RFWJaFG4u1cUrbuEFrShMw5rusnmYI89OtVi3+/8AzQVxz866VatCV2qCg4UVv9muyr4o53Jt2d5+8w8ugPX4Vilf35cvpRtvtzes3FECEVVAjwlUQKDpz+kVK6cclvb65wnh6Wk7uzbCIPQT5k0Ybh9nrppSn2f7b4fEQGbI/Rtj6GmzDa+N9ByUch1Y8z5D51I1lLPb8541XS8yOCHQ5COeYGCPjR3csNG3HKR6H56VofxU4vYuY6bFsK9qBcuSf5jrBErt4QAJ3O3IUtYrj5KsBbylhlJzToTJG1Yyxt9LhnJLtrXm0iNaBTCNcdU3LEDTU6mIXzrKt44nSSPWvrP8MuyhA+2XxB2tA8p3f1jQeprPjcXTzmR4biqWbKIQECoFygzAAiCaxBxfPmuzks2/buH2R/QoGrOeQHyq7iPBkuHvbtzLh11Yzq5/Cn6/DyRu0/FO+ui3bGWzbAFu2PuA6bD7zbljrBHrUmFvefoyzk/rh7G9o+2V+94UzLaaVRNJMbs7bk+S6DamnsstnA4XK9wG9d8d5x+IiAo/pUaAevWkLEqFdbawe7UAtyLTrl8gdB6UdxTEBIBMmJqZcmX0uHFjPZ34v2vti33dv+YSIzHaszhttchuXboQt7KiJPupPTGggK6+8b0DezI5IYleUmud3l7dZJjOjfxHAsz95Zcgx7LHwn+08vTb0oS5cW6Vs3EzOTlKkdfyrKwvGXAABknat/A3gZZyudlIDQZXzUgg1jX63v8AGRx3skqH+Ue7P4GJZJ8mPiHvmljF4J7Zi4pXodwfQjQ0+Y64h9i479S8TPupR4pjGuuQuoRci9M7HX5anoFr0cWeVunk5+PDHHc6rOGEeM2UxuDyjqKpy0TisSMwV2MCZUTJAO+hAnULqTEaCqsKO8doHdoomD4jEgb7k6z7q9DzKStckVoYjAlWdR4sgUmBE5toHOs+4JOX3nqBQU5Z126VW9uiXFcGgDdKoe3R7Cqnt0AXdV5RZs+VSiPpXDuzfAcYuWzfi4dR/MNu4D5Wrgy/9pqy5/Cu7aR0s31vI2y3QUZRzGZZUz6Lyr45dsEaMpHqK2eDdrsdhY7nE3FUfcY509MjyB7oqgheDXLeJu4a6RbuWpJBPLKH9oSPZINbvY1wLlwcyoj3Ez9RS7xrj9zHX+/vKi3Mio2QEBsuaGIJMGCBv90UTwPi32e4Lhti6hGV0YkZlPRxqpkDUVCGDjOFYXGuBSUOpMHKDAkZtp0G/Wgrbg7fD98q027Q4R3HctfVmVoDZQbbiCJdT41IDchHOr0wl+6JNpXB53Asn/UfH7waKywOdenTf6US+Avp7eEfzNm4twDzKv4yfIHlQIxdhmKm7kYfcuo1tgfTWPeag6IJ8pqjFICIIEch099aLWGOoGYf0kMB6lSYqvDYK5ecW7aM7nZVE+s9PfQLt3BldVPupg7O9t8VhvAWzp+F9fgdxT3wzsBYw6faOJXVCjXuw0KPJmGrH+lfnXzjtXi8K15hg7b5D7Ocyf8AQu4X+4k+lNNY8ljI4v47ty6AQHYt1idSJ9aBtWGZlRRmZiFUDcsTAGvUmjrGHuKPy3HvrQ7PY4WcUtzulLj2FcSobeV84BA3jNpU9RrrI4cO/hvh7ItX8TcuXIAZraNZVGfcKHZwSAentRuBpW7/AOoEvFLl4OuGW5C21hSpClT3pVtUkka9DtpKxxDvXulcXhBbVwCiIqo6Hk4YKWPow1Okcqu4FgLly4GywFgMpXcxEROw0+Arhllft6OPHH6G9v8AtBmKhGIGoClMqoFiCgk5pB06RzpJwt7WQdZ/ZontwAl1LK7Ise/ePhFZWEY1q22brMkxuo0jfIIIO5j3Cf376JFzNqTrQCPO/KvEukb61jTptrCDXf21RcAYApsQRNZX2odarN+TTS+RmfC4d9bYynrQNxHtnUkr1qjhnEFTRkDDzqrjHaXvSURQlsDWIlj0np+9RoU47amXLjjHGP4ixBUEhfvHm3kKCN0hTcCQo5wSqjz6nrty2rjBDvCXf/hprod4I8IEyJJjN689rftOYXEC+BlkAknJEaA75T02Hvr044zGajxZZXK7oK7b2YEE7iDIOmtE4O7klgAxIgTsPdQtgZTkOx1Xy8qItPlYGJI+Pu5VUW2w2cIxINxSN9hBiPSZHpXd7FTIjTvQDMTGWdDyGtcXL4N1HE6fHaDQ2IUi44HN1j1AoLWjXkBv06VUY5VMcdRaXlq58+lU3GC+Eatz6Cg7Irg1wL9WFwTpoOhI+sCgrqV7dUqSDoRyNSg04B0IBHQ6/KhL3B7TbAoeq7fA/lRy267UUGC3BHQyGUroJ1BEmBI6SRzoRMRPh2O1NrWsylZ9oEemm4pT4hh2t3iGiSMwjzB+h+lEMWD7U2cPC2sILmUQGuNA03bIo1J3zFuewo+x/EZ5m5hxl6o5+QYQfjS92YwyvdhxKAFmESCoU7+UlaLxvB1s3hZLTaxGiE727ikRPXVgs9H8qK+i8A7U4bEwtt8tw/8ALueF/wDTyb3E1t47g2HxK5b1pH9R4h/aw1HuNfAL9hkYqwhlOo6EU3dme3V6xCXy161tO9xPQn2h5HXz5VnS7/THxrsbctnPYPeL0MC4o8uTe6D5GjsB2yTAWmCHv8QwjKSe7tAbd4+7Pv4Rttoa1cJxy3eth1YOrbMPhrzBnTWsXjXC7Nx+9ygP15N0zAbnz+PKLs0WeIYrFY9+9v3CRrBI8K+VtBoP3qaIscMS2PCIJ5kjMd9z+W3lV17DBZYP3fUmMp9Z01oWxxR7jizZtd7eYwoXRT1OokD5edEU3sMWIRAWZjCqupJPIAbn0pz4RwG1wxBiMSFu40ibVvQrY03PV458uXU94HEWeGozEi9jIh7gA7qxO6WydyBGY76iYECkHj3aJr7EKSVY+J29pzzidl8tzoTyAukNQ4wtzEMcSxLMAc2ZMpU6+EzGmoPPQj1MxvHipyYRFbMp1kgaKSQBzMCk/BYgrbV40EgSOakMQP8Af86YcQ2UpcTTQMPzH5ehNeXkt8u3v45PHokY28X1YkkknXkedWYZoAmtS7wVr+LFi0VVrplSxIXNBMEgHfQe+g+N8HxOEbJiLTW+QJ1Rv7XGh+M10k3HK3xy7cteUUJc4kg5E0Pdtk67+lDXLdamEZy5MvoTd4ih5EVXbx6jXegXWuIrXhHL5KMu8SJnkDy8q2OHcEuNbS6yk5zKWhu4jR3cnRZgxBkDzoLs/gAzh3gKFLakbeIDT+5Y94rVx3GblwFkcqCcrqCISBAURyMcq0zvftzxK9A7rMrQZYoIQQIVEHRR8SaqwKNlZhABWNdjJH6UG7KNCfdRGEwzXGA5fQVRZiLYPhOhGx6etSyhJyspLeXPzq3iJHemPKhW4v3cqje4cvhUFuISD0NcHEAMbkT0HU7CgUV3MmfVtAPRefvrjE34IjmJHxIn10oLWcjQGXbVj0mhnugeFdZ3PM1Sznbrv51LOhoCmTT6VwNKsXSvFE6DWdBQd3uJsDAkgAD5CpW9huGoihWEsNz5nX86lc/ljtOHJEWrFWuVSuu9Gw/OtuSyQNfpWD2qXW23kyn0GUj6mtlfmf3/AIrF7UnW2vkxPxAH0NEoXgQU3lVnNvNIVwD4WgxmA1y8jHImtvt/lH2dBet3GZGuXBbIZULEKFzAwT4XkDy30pZwhAZSTCkwTrsdDtyjl0rVxeDB0YDUSrLqCORU7EVUXYpPtWHF/e7b8NzqwA0b1jX/AHdKxl9dfPbQVo8IxDYW8C0d2/hY8o5E+h+U132h4X3VwFR/LfVeg6r7j8qALhHFbmGfMhkH2kJ8LfofP8tKfsBxdLyBk22IO6noaRUsqULmJGkbe/3aVRw/FvZuZlMcmB2I6H96UsWU88bwouqDoGUeE8vfEmPnRxxVnh1vucM3e37gi5fXRnkTksz7K9WO3nzz8FihcQMu3Mc1PMHzrnE4fd1HiAjziZge8k1IUKnCze8VxvEIyqBNtVmcuU7idSTJJ1OpqvivEGtW0RrVq4DMQRlgAeyo2Iqq1xtEE7zsB+/3FLTDc+ZPzqoY0xWbB2AeT3pj+pl/KPhTLgjntAc150m4a/NhLZ3BY+8kfkB8K3uB4rwgcxoa83JHt4r1FmKtOsXUJW5Z2I3AmVYeasB7iOlb/CP4nMydzj7KYi2wgnKskf1IfC3yrJxTaE9QQfMEailtsPppW+G9Vy/kTuHTH9kcBiwb3DcQLT7my5JX0g+NP+4dBSHxDBvaYpeSD+JSCp9CNPjrXXiQyCVI2INdXsSX9sknrvPqP36V204zKxn3cNIka+n6UC6RWscNzUx5jb/FVXlkeNf9Q/f1ot1Wb3pgLyn4+vxPxqzCYw2nLAAg6Mp2YdD+vKurmG5r4gOm/wAKqRABmbXy/X9KMGbPZuBe6VUU6wd/PMSd6su8QS2MtsZm68hWEbJA9uBud5JPP/x0quxlGpYkA7eLxHpoZ+FFEu2doOZyfuqDr7hqfpVww7LplS35SC3wXT4mu7uIYkWlC2vxhABHWY1PTUnWrlwqjUsxHJdJb3xoPP8AYAUoWi2s67mqeKWwAscpH7+dbFmFzGACBoBsv5++s3ELKnSYagyVUnlVlq0RqaMCnzqm8SNxQ1UgkwK2MBaSwc1xgbnIaQn+fOsezdEz5fOig1TKb6axy8bvTaOPT8Q+NSscGva5/FHT57+N7id6w11/s3eCyDoX+91KTrk6TrQwYf4+NVW3/wA61ahA9DMzttz0/fzro4ukfXTU8v2PdWFx9v5sHZVA9Nz9CKYVMxyERpPyFLvFNbrnfWPgMv5VQ14DsdYuWM4S4+k5kugP7gVKe6PfSf8Aau5d7YzNaDey8Bh5iNFb00PMUbwVLg/4N17TEkDKxCnSdV2NZ9/C3GVrxgmTnGzK06yPnpRGtau2jbLE5rZ301B6Ecj+xVI42jYZrDoSQw7ozqFnST1G3vrM4ZjTaeYDKwKOjbOjCCD0PMEaggEUThcCjMwMgDYSPqJ1iNOs7kQaA3tkCeU/uaLXDwguP7J284008/lpWpgcCFIt35C3BmtmILprqB7j8NJBFYWLlTkklVOnp+tQHcN4sbVwfg0Vo3ZRsf7h/ivoCWgyh1IIMGRzUjQ/CvlczTt/DjjYVjhbuqsCbXru9v8A1CWHmvnVVm9rOE90/fKPA51/pff56n41hJrvX1HjGEV0e0dVYaHy3U+oMfCvmV20UYo3tKYNEeq5Gora4beg5hsd/KsGass4lkPUdKxnjt048/E3Yy74TyHWgjPPX961hcT4obihAIUanqT5+QoKzxB10kkdD09amGPjF5c/KmRxIMe+hblnpVGG4krCJ189/jz9KKe5POevWtuQQypkaelW2MeVIPsnqPzX9+lU3rkT+/rQrXxRTLYwmGxA/wCjc/Ha1tk/1WzEe7LWXxvs3iLQLsme3/1bfiWP6huvvHvrPtXIMqYPloaYeC9q7tk66jnH6bfSpoKbXCxAnyovCW8hzNuNp5Hr601cbwmHxatfw5Fq6qMWRFEXSBIBEjKxOkxrO1J+Huxy0NVGhhDJJCFuvKfWTtRfeGZjX1rNsSdTp6UaH60UQzQhndj9KFtjwnzrstPuqXDrA2igrUUNjUzH0owCqnSsx15L1pnQR7q7t3que3VDW604i1u+Y99Sgfj8DUoGi1h59jc65TtpvlJ+h0ruwcw9N+v7/SpUqKY+zPZd8WLji4LYRguqliTE7AjYEUiYtFC55nNqNOutSpVQ+fw57FDEIl25cKo1suAgGYEv4dWBHs+VB9vOB2sHigLDs4IQX7be0SwOVw0BeYEetSpT6Pso4fhSklhOXlJ/elbVzBJhLa3Lqg3Li5raHVVQyA7xoToYX49DKlWeilviOPe46uzFnkGT7o/LyAgULeUqYYR8DUqVBy1vmK4S4VIZTDKQQRyIMg/GpUoPpGD4n31pLnNhJHRgYYekg0r9p7ADi4PvCD6jn8PpUqUVi14Xj0qVKqOGUHb4UOwqVKDiKus4tl5yOhqVKiCbmLDDag3Y1KlFeZzV1rFkb6/WpUoDsPe2ZSQRsRIIqrEjWSN9dNPkNK9qUHuFeiJ11qVKKtDV2DUqUHSGvHWpUob2qZapdalSgpNqpUqUR//Z"/></div>
  	<div><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGwlOHy78h8p9TG2lzG_8tLlVTRwnWoSC3HXgmKcAifaB-JL90hg&s" width="100%" height="40%"/> </div>
  	<div><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFRUVGB0YFxgXFxcdGBcYGxcYGBoaGBcYHSggHx0lGxgYITEhJSkrLi4uFyEzODMtNygtLisBCgoKDg0OFxAQFysdHyUtLS0tLSstLS0tKy0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0rK//AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBgADBQIBBwj/xABDEAACAQIEAwUFBgUCBQMFAAABAhEAAwQSITEFQVEGEyJhcTKBkaGxFEJSwdHwByNicuGCkjNDU6LxFoPSY3OTo7L/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAAhEQEBAAICAgMAAwAAAAAAAAAAAQIRAyESMRNBUQQiYf/aAAwDAQACEQMRAD8AWuF9msLZxNi9cxS9wHBIuCHkAlfZJDePLOg0mnv+HXaZ7t7F21e13rOe5S4NTpE51PiULbGg8jsSayOyOAsnD27tq2isV3iX0JBBcidwdo0iu8XwKzhbd7iLE9/cv5cMFJASbWV2I5nxO3+lOppJ0tV/xbwt1XtNeyG5esQ5tzkLqSCVnWIKGvjxcczX2LhrpjbIt4y4Vs8Pm81wTmOHZTNoEc86KBziQNQKJ473+GwwxGBw1qyt5VuDCmwlw9yoIZ2MSbgm2XUaANMt4jUHybgPGr2DvpiLD5XQz5MOasOakaEfnBr9I8I7XYfHYRcTabJcUarqTbePEjEDVT15gg7gR8r7FWbXEnbDXMKljELbLrcS3Ft1DAZbtpho3i0dYkDXYS29i+y1/h+IbEWRmsuMmJsg5tBJW7bG5ymfDzUtGoikHT8cWzie9YlrF+0wWBLBwyt3RjQrrcIzTl8YBApGxfaO2cLeweGtXLK3GL+O4rZVEl1tjcFhOkka6Gmjtrdw2JJGHuKVaG8P3GkAOPeQD6mktMDbfDuTaW25Ym25k3QkwC5OxI3UCPIUpHX/AKmQ5e7tlEQKtvUSAoAhv3t76X+1Kg3u8H/MEn1Gk/DLR/dWTkAt5CQFYAnRgcrTPLnPpWTxowwXfLt8KgzqlSieGYcXL1q2dnuKp9CwB+U0RvcA7MI6pdxTtbS5rbt24N64PxAQcq+cHfkIJb7HCUW+9nDWSEteEopZ2VwgZnuk6sTMBhIGo054WI4hrduzFy5cy2wFJIt2SGW2sGVkkCBM66ayGbtBhb2HJVi1t8T3d4ZSwzOqBHtmNZEBgNdzRoHw7te2HvuVuFrTNDDXKAQOoBzCDyHv2q7iPau7dbKRmXkD57Qf3pWPe4Xed7irkJY+IKT4DOYBmYASRpAn51mcRuFVuJuUUoY5lZWfTelHnF+PZAVskF9ZcbL5J1Pn8OtedlLwFi87t/zASTufAT7zSuRA+n7601djh/Ic8xeEbaEIDz91EbnD8SLttwsnwwQQQQcy7g+tKfb2RjbhYEAhCpPMC0gkddQR7qdOF2jnu3GYu1zUkgDXMoAAUACre1var7IFtL/MdkByEnIoiMzDmTG3ONxRXy3E4drbFHUqy7gxppPLyNVURxHGvfuveuEF3MmBA2AAA6AAD3UPRlKlSpQSpUr2KDypUqUEqVKlB7UryvaCV5UqUEqVKlB9S/hpj5RrRPst8nWR/wByN/uozt5bMWtTlBYRyDMF19SFj3UodkXuWMTDAgMMsHSGUhxPwI99OnbHEzhLzAAkLIkezBHiXoQJ+nOtSqo4hwuza4J3lty/2u9bS6Z8OW1ffwgcgDM9afeJ3Bi7PcW27nGWibmHndbtrKAQdijq4XzW6dJBA+S9m8d3/BsXgyfFYuJeTyRyA0ejqf8A8lM+B7T2Fu4e8zXUuXUXM+rW20CG0VnwgMFaQNCfM1A8dlu1qYqwbrDu7loHv0O6OoM6Hl0/UGsj+H/EnucPs3Q2W6e8OvMtcdiD1/xNLfabG4O1dxb275XE4iw9q5bAJt5yNGYqvhfyJ+8SYmax+z/EkSxYRSBewzkspfJNtvE5B9lgQF0PnsabFPaHD93jPtCjJ3rlL6jQK7+HPG2pIJ8wDzpX4ncuJfYW2ID+IqxlZbfQ7CZEU7drm7y13rRLTau5QQD+BhPlpMnWINJHaWW7u5zYax1IDQPfmFRatt4sEFiQsRI6sBGnu0novWsPFXs7luv0ogYG62+n9xj4jf41RewjLrEj8S6j40RTVuDv5LiXPwMrfAg1TNMOB4XYs2kv4zM5ujNaw6NlYpsLt191QnZRq28xRD3/AAs4Qt7F3L1zVMIcyj7pdzNs+cZWb1CHkKcf4icTwqYeMSucsZtKph84+8rfdA0k+fMxSV2f7SXLeGN7Drbs2wR4VRZgNkJYldYjz9a18dxfDYsZOIYZGMQt+0CrqJnWNYnUwY8jV3qLoj2+1LLmW3bi4dWdnDQYAkQokwKzr7CPVZ940M+utd8Y4SMNcY23720dVbQmOU8jr5f5zcVioVRP3Y+NRWWzU+/w8shrKgj2sS4PoLFqPma+f0//AMPbuW0vliLh/wD02R+VWIdOL4VUt+Ea51HumT9K+S9rrhfH3xqWNzKo5kABVAHmAI9a+s8RxAZVHVx9DSx2s7XCy/cqq3XWD49Utn2hHPNz0I3pfa/T5s6EEqQQQSCDoQQYII6zXld4i8XZnYyzMWJ8yZPzNcVGUovh3Drl5oQaDdj7K+p6+Qk1xw7Bm7cW2NJ3PQcz++cU9YW0qKEQZUXSObeZP586lrWOO3HCuz2GtCWt/aHH/UZhbH/t2yP+5jPlTFYvWlEDB4MDoLC//KszOAIkDpXa4gyBmT8zt59ZqOmpBWJ4bgLwi5g7dsn79jwEecLE/OlHj3Ys2wbmGfvk/CdLg+k+kA+tM1q4xPKNZj120PL0opdifKm08ZXyKpTP2r4VDd4o1bXT73Wf6h86WK052aSpUqURKle1KDyvalSg1hjmDi4zEkMGJ66yafb17vEKHZlI8oI/zXzem/gl3PZQ6yoyHzjr7qNMjsTYvJiR/LY2nDWbxMKuRtCczQPCwVtPw0WMR3Vw2wwYKxyOjCJnxZTqpBgGDzB2rP7QYnLcKuzNEFV1gCPPSu+F8OF+wWtE94rHMhiOqlDpGnXmDRGjxK3ZIvYm6bhVmnu0hTLmDLTrB16etZ68LXvgpuHxKDaeNCIJAPn+hoZ7xe2bTbll+E70bfxNq6oVHAdfZmQZHSfSij7VwmzctnYofusJdNBMjeVH71rAa9nw55lCfrmnT+5vhTAmPm0WOkKc3qBrSzwldGU/eUH4afRiaJXPCXbvBE+Yn9afzwdGt94DlaPbAEg9LinQj166RvXzdCVYEEqwO40g+VOHB+0BurlaBdUa8luL1jr9PSRUqxVi+A2GuKwGQqwNxN0YbnL5Ej5kVjPZfFMz54JPhLloKjQLIB1URptryrfuXAWAGxDR5GDI+XyrK4Iwi5bYSsgxsRoIKnkfOkK3MEot2O4BJUIw15kksTHqTQGC4mQiggsUOVjI0A+8flXOd0YLrcU+y4jMP7x5daAYBbrBvZYCZ26DX4a1RrNeknqDMSD4eR06/uRrWB2g4f3bB1/4b7f0np6cx7+lauYA5ZjTwCeXPMdBGhiZq6xkuLkuLmTQ5Q0GBqQGGxB+tQJ1OHZG4wsQiF3N1sqjSTltgyToNNfdV2G4Zwq5uMZa/ta23/8AU0ecJhrGW1hbly8rSf5qgNnOUFdABEBdfM1SRpcOxvfKukMl0BxMwRmUwedfPuLYC+2KdWtPnu3HKBgRnBYsCpaARl5jSK+g4BEQoiBQCR7IgE+KTHnWB2i7a3ld7Fg5UUlWbeW2JUEwIOkwTpQI4Ne17FSKMtzswF8WviYhROwGpmf3sKZcFi0ZSytmAMenl5Ur8DugW7qnmJ+X/miOzbAd4CYkL8if1qVvGmUqGOuoHLX8vrVyW10EaAz79f1rNsX2LrbQZixhfInnPIefIUz3eCMR/Lvh2G4y5TI9ojWIA61G9sy1hRpuY8/T9KJzQRXWJU20hiAZhgojXqT56kRFZ7YjkPCPKibE4zDqw7skDNqp08LDY/P4E1834rh8lwjYHUeXUfGaZ8dxp0v90oTICBJBzEkCTM/1bRyrO7XYZluKWiXXOY5SSCPiDVjOXZeqV7XlVhK9rypQe1K8qUBweda3eyt3xOnUAj3f+aXrG1aHB7+S8p66fGiwd2p4a9y5ba2hYlSDHKDIknQbmiuzPC3w7m47AyIKCYOsglvLXYczrW1duhQWYgADUnYVgY7tENrSlz1Og+G5+VALx9Ct1nAAzHOAJgE7x056eRrOTE4cwz2rgeQWyOuRjMzBWV9AfhR12615FuTMAq68lMyGX5T5Gsc2taDR4hxNXGVFyoxljrr855CfpV/2pSEUgSAFzCdiI5jpWStvQr7x60dwxxBBHOfy/KqAsTb8TGJ5n86o70qwddCNR5fvWiMePGRMD9dRQpcgwdYqDXu8RgrcG0gkdOvy0rvFIVdssw66RzI0I/2xWNdOgitHCXs9vL963qPQbfDb0obbeFBVGziD3cKZO+mvLWJrKdmUwwZkYEeI5iZETG4ru3bLEugJJ1Oo0/Fmnl+vxvxJzIRI20gg6jUQR5iigbGKZQFOo5Hn8d6MuXcy6ctarsgOgJ3jf/NRBEz035GTQcJxJFYghp8oj60w8Kcd0+J9bVqfxuBnb/TbB97rSTdlnMAkkwANydgB6004l8j28Gmq2QVZgd7xOa83pm8I8rYogvjOOZLSsjQwHhI5HKP1pJj48zTRxggWiMoM7Ez4dV2pdyUKHIqCrjbrzJ0oNLs4yZyHWSYIPSD4iRz3HwphvYWzbZgECtzylzm5g+MnLodh1qrh38PMdlF+53WGQalr9zKQJgyqhjqJ0MU3YTheEw5F7F3TiMo/lWhbZTcIOneSSMuxAJ1B1ECDm2b1tuY3W9MTheAyZcTd/l24JRfvXRBBM/dt66vz2UE6g+zxZf8Aiq6o0EBEUqzayA3IJ/TruSSSdMrtL2iuYu8LlweFdFtqICqAYI6QY1O+vkBRBWAVk88pOUaT7UH4xr5VSX9GfbLjEm7DZjJA2EaCI2gaD869e3zGo+YoNMUPFIK5GCkGDqY2I33otXg6Gopf4lhX+0E5CcxUiAYIyrz9xn0qvtPfzlD/AECfWSfzpoOPZZykqACTqcpOnLYwJ98Ul8Yxpu3C58gB0AED5CtRmswiuSK6ajuE8JN8Oc4TLESCQSZ0JGoiOh3oyzTUrVv9nsQuyrc/+2wJ/wBph/8AtrOuWGVgrqUJ5OCp+B1oiupRRwR5MpHrUoJZYk686u2IPQzXiW2JLx4RrJ0E/h1+8eg157VoW8E72muqJA38j50U3nDd7YP9aEfEUiGyRuCCNwdwf1r6P2LdbmGTqJHw0pP7SZbWJvK7a5ydd4MMPkRSNZLuztq0WPe37dtYE5mUa76yZ2Ma1mcWw1tL1zu7lt7cypW4p0OsadDNDrj1nRZ9QKrxmLY7AD0FVkGcSegojAXZaOZ+fpVIsO28++juG8ODOoLKms5iYGmvP/FQV4+1JHn+/wA6BccjuPnTNxvh72gGKRDSCRKt6HYj0rG4ljnbQ27Vv+y2FJHmTr8KoAK7L+5q+xKGaGozBKx0iRURqYjBlfEAGVokQI+fOu0QkeFZjWANvUbij8cYsP8A2gfMCl23xN01lTGxMae8Hf1pVXW8DecBURmTUaDwgyfabYadTyqYgZF7pWD5faZfZJ5hTzA2nnrXj8QuXvCWOswAzR79Y86IXCwAOgoPOB28jNfP/JAKed5pFvTyhn/9utPgGDmX1J2HXqT+/Oub+DfNZwdtZcmXA3724AYI6IgUHp4/Om3thdweFWzhcIWa6ifzmMxJ1Gb/AOpJJjYAwRtAKnHuScxv8Z/Ssc2qPvEscx1JqspQBNaq/hRyX7Td2buV1IQAksZ0AA1JmCBziKKweDe662raF3cwqqNSfpHmdBTpwrFYbhRJhL2LKkG7ulox7NoR4uYL6SdBpMtBn44xs91exdwG7MrYRQWAIicxYBTMHbqNeSJ2puO1g4hnYnONG3ZczL56iFJ+lV4fid3EHE3bhk+CMwB1LMZkjeF5RGwihe0OHY2VJYsFafLxaGB6kfGvPqTOSPXLbx21TgcWlwa+11G/+pefr9aLbDTHstG0NBjoAYPIfClsW41FEpjLg5z616NPLK2ThgAJEANn1O7dSTvXfeAanbnyHxNYrcSflA91BYm+7e0SR8vhU0uxPGuL5/Ans8/PyHl9axcxq1krgpVZcTTL2Wxtq3bYXGXVpg6ECI0OxmNtdqWyteGgYh2htsTKMonTY6cp21ijbHFEYZRdBB+623+1tD8KTya5mgc/sNr/AKFo/wCgflpUpSXEuBAY1KDQfH2hqFzH0H1q7A4g5YBMGCROhIkSR7z8aP4h/D/F2ZJVboHtdy2ePUQCfcDWTbTIQBt+x+VA1dmePPhCwCJctOZKNIynqjD2SYqdpsdYxIvObQth0U22lWYXbcyO8yg5SDly8tzvpj4cTC9SB8TFOXbvBKMNaKqALLAARoFIj6haD53Ywo5fE/kOdELYA1+tdm5XmpjSqPSorhtNudECzVi2PKoBsPjriAqpOQ+0u6t6qdDQN+zmMkE9OQHkANBW4uGHSre6UcqDDscLG5mPOr7VwBwpIROoGp8s0HL6wa3OHcKvYp+7sJmPNjoq/wBzflX0Ps//AAvwqwcRmvvz1ZbY9ApBPvNNtTG+wPYjsfbe0mKxKLdS4xyISGthFn+Y0aNMGAdIj3a1zjKXHOHs4JQrAqrIFFwLqMygKQgkQJ56emTxHtkiWkwmGUJbFrL6ArGVR6GsrhPGfsoa4sG5cJjoi6fMkT7h1NcOTLt6ePCalEcR4Rg8TeYG0cLlYILoyi7duKP5iXFJCDLuzRy03rDxfBksFry3BfsWjLNBXxiMqMrcmYqMw0Ou1XcW43dusWuEGfIb+XTaq27YX7dj7NZCLbOrsVUlid5J3/TSmOdM+Oa/0H2b4z9la7iYL4u4MuHBEgNcJ7y8fMDRRzznlQFxEygB+8uuxa60zHMieZJOp8jTZwbhv2TB3uJsM124pGGJUlbQueHvDpEgE68gI+9Q/YHsdbuYdbt5nIfZE3YbS78p6DXzrrc5Jt5phlbqFpLDO2VEZ2/Cilj8BWphOxuPukBcOVn7zsqgebayAPT0Br7BwrhJsrFqzbsICDJEyOZaNztuax+Nccv4y6cFgY0EvcJhVWYkkbSdBGp1jYkTHK3v01lhMet7pU419m4bh/s+Hu58U+l65oCw5205qswSAZManoi4gmZeQT+LT619z4HwnCYVMrWz3u1y6ykl2H9Y2XeFGg+JrrjnHUt2zFlYOzLqB66SPfUvLpqcNr4/wFA1jEHkCpkdRMD4mjcfam1HW2fiACPpTxxLhjXLKl3VhdAbKOnIkik3i47oZW6bn061wuflk9Uw8cNb2VQlQ26JFuuSlet88GVrhrdHlKrNqgBa1VZtUebVcFKABrVUMlaLrVLWqABlrgijHt1SyUFMVKsyV5QfdOH8asi0cRhj3oQg3bKHMyp950XeV3Kc1mIjVe/iZgsNdtWsfhgsuwDsnsurAw5H4gwAnfxa7V874ZxO5YdbiNBUyCDuNJB+A9CJptxuND2nIEW8SPGkQqYiZS6saAOyhWA0DQfvVNLvZds3CCCORB+GtfUeOYY3cI5KyrpmRhqrEeIQdtxXymw8imHs5jsWCbWFvMmYHMCQbccyyMGWddwJqkrPfBMjZXVlYAGGBB1EjQ+RFWJaFG4u1cUrbuEFrShMw5rusnmYI89OtVi3+/8AzQVxz866VatCV2qCg4UVv9muyr4o53Jt2d5+8w8ugPX4Vilf35cvpRtvtzes3FECEVVAjwlUQKDpz+kVK6cclvb65wnh6Wk7uzbCIPQT5k0Ybh9nrppSn2f7b4fEQGbI/Rtj6GmzDa+N9ByUch1Y8z5D51I1lLPb8541XS8yOCHQ5COeYGCPjR3csNG3HKR6H56VofxU4vYuY6bFsK9qBcuSf5jrBErt4QAJ3O3IUtYrj5KsBbylhlJzToTJG1Yyxt9LhnJLtrXm0iNaBTCNcdU3LEDTU6mIXzrKt44nSSPWvrP8MuyhA+2XxB2tA8p3f1jQeprPjcXTzmR4biqWbKIQECoFygzAAiCaxBxfPmuzks2/buH2R/QoGrOeQHyq7iPBkuHvbtzLh11Yzq5/Cn6/DyRu0/FO+ui3bGWzbAFu2PuA6bD7zbljrBHrUmFvefoyzk/rh7G9o+2V+94UzLaaVRNJMbs7bk+S6DamnsstnA4XK9wG9d8d5x+IiAo/pUaAevWkLEqFdbawe7UAtyLTrl8gdB6UdxTEBIBMmJqZcmX0uHFjPZ34v2vti33dv+YSIzHaszhttchuXboQt7KiJPupPTGggK6+8b0DezI5IYleUmud3l7dZJjOjfxHAsz95Zcgx7LHwn+08vTb0oS5cW6Vs3EzOTlKkdfyrKwvGXAABknat/A3gZZyudlIDQZXzUgg1jX63v8AGRx3skqH+Ue7P4GJZJ8mPiHvmljF4J7Zi4pXodwfQjQ0+Y64h9i479S8TPupR4pjGuuQuoRci9M7HX5anoFr0cWeVunk5+PDHHc6rOGEeM2UxuDyjqKpy0TisSMwV2MCZUTJAO+hAnULqTEaCqsKO8doHdoomD4jEgb7k6z7q9DzKStckVoYjAlWdR4sgUmBE5toHOs+4JOX3nqBQU5Z126VW9uiXFcGgDdKoe3R7Cqnt0AXdV5RZs+VSiPpXDuzfAcYuWzfi4dR/MNu4D5Wrgy/9pqy5/Cu7aR0s31vI2y3QUZRzGZZUz6Lyr45dsEaMpHqK2eDdrsdhY7nE3FUfcY509MjyB7oqgheDXLeJu4a6RbuWpJBPLKH9oSPZINbvY1wLlwcyoj3Ez9RS7xrj9zHX+/vKi3Mio2QEBsuaGIJMGCBv90UTwPi32e4Lhti6hGV0YkZlPRxqpkDUVCGDjOFYXGuBSUOpMHKDAkZtp0G/Wgrbg7fD98q027Q4R3HctfVmVoDZQbbiCJdT41IDchHOr0wl+6JNpXB53Asn/UfH7waKywOdenTf6US+Avp7eEfzNm4twDzKv4yfIHlQIxdhmKm7kYfcuo1tgfTWPeag6IJ8pqjFICIIEch099aLWGOoGYf0kMB6lSYqvDYK5ecW7aM7nZVE+s9PfQLt3BldVPupg7O9t8VhvAWzp+F9fgdxT3wzsBYw6faOJXVCjXuw0KPJmGrH+lfnXzjtXi8K15hg7b5D7Ocyf8AQu4X+4k+lNNY8ljI4v47ty6AQHYt1idSJ9aBtWGZlRRmZiFUDcsTAGvUmjrGHuKPy3HvrQ7PY4WcUtzulLj2FcSobeV84BA3jNpU9RrrI4cO/hvh7ItX8TcuXIAZraNZVGfcKHZwSAentRuBpW7/AOoEvFLl4OuGW5C21hSpClT3pVtUkka9DtpKxxDvXulcXhBbVwCiIqo6Hk4YKWPow1Okcqu4FgLly4GywFgMpXcxEROw0+Arhllft6OPHH6G9v8AtBmKhGIGoClMqoFiCgk5pB06RzpJwt7WQdZ/ZontwAl1LK7Ise/ePhFZWEY1q22brMkxuo0jfIIIO5j3Cf376JFzNqTrQCPO/KvEukb61jTptrCDXf21RcAYApsQRNZX2odarN+TTS+RmfC4d9bYynrQNxHtnUkr1qjhnEFTRkDDzqrjHaXvSURQlsDWIlj0np+9RoU47amXLjjHGP4ixBUEhfvHm3kKCN0hTcCQo5wSqjz6nrty2rjBDvCXf/hprod4I8IEyJJjN689rftOYXEC+BlkAknJEaA75T02Hvr044zGajxZZXK7oK7b2YEE7iDIOmtE4O7klgAxIgTsPdQtgZTkOx1Xy8qItPlYGJI+Pu5VUW2w2cIxINxSN9hBiPSZHpXd7FTIjTvQDMTGWdDyGtcXL4N1HE6fHaDQ2IUi44HN1j1AoLWjXkBv06VUY5VMcdRaXlq58+lU3GC+Eatz6Cg7Irg1wL9WFwTpoOhI+sCgrqV7dUqSDoRyNSg04B0IBHQ6/KhL3B7TbAoeq7fA/lRy267UUGC3BHQyGUroJ1BEmBI6SRzoRMRPh2O1NrWsylZ9oEemm4pT4hh2t3iGiSMwjzB+h+lEMWD7U2cPC2sILmUQGuNA03bIo1J3zFuewo+x/EZ5m5hxl6o5+QYQfjS92YwyvdhxKAFmESCoU7+UlaLxvB1s3hZLTaxGiE727ikRPXVgs9H8qK+i8A7U4bEwtt8tw/8ALueF/wDTyb3E1t47g2HxK5b1pH9R4h/aw1HuNfAL9hkYqwhlOo6EU3dme3V6xCXy161tO9xPQn2h5HXz5VnS7/THxrsbctnPYPeL0MC4o8uTe6D5GjsB2yTAWmCHv8QwjKSe7tAbd4+7Pv4Rttoa1cJxy3eth1YOrbMPhrzBnTWsXjXC7Nx+9ygP15N0zAbnz+PKLs0WeIYrFY9+9v3CRrBI8K+VtBoP3qaIscMS2PCIJ5kjMd9z+W3lV17DBZYP3fUmMp9Z01oWxxR7jizZtd7eYwoXRT1OokD5edEU3sMWIRAWZjCqupJPIAbn0pz4RwG1wxBiMSFu40ibVvQrY03PV458uXU94HEWeGozEi9jIh7gA7qxO6WydyBGY76iYECkHj3aJr7EKSVY+J29pzzidl8tzoTyAukNQ4wtzEMcSxLMAc2ZMpU6+EzGmoPPQj1MxvHipyYRFbMp1kgaKSQBzMCk/BYgrbV40EgSOakMQP8Af86YcQ2UpcTTQMPzH5ehNeXkt8u3v45PHokY28X1YkkknXkedWYZoAmtS7wVr+LFi0VVrplSxIXNBMEgHfQe+g+N8HxOEbJiLTW+QJ1Rv7XGh+M10k3HK3xy7cteUUJc4kg5E0Pdtk67+lDXLdamEZy5MvoTd4ih5EVXbx6jXegXWuIrXhHL5KMu8SJnkDy8q2OHcEuNbS6yk5zKWhu4jR3cnRZgxBkDzoLs/gAzh3gKFLakbeIDT+5Y94rVx3GblwFkcqCcrqCISBAURyMcq0zvftzxK9A7rMrQZYoIQQIVEHRR8SaqwKNlZhABWNdjJH6UG7KNCfdRGEwzXGA5fQVRZiLYPhOhGx6etSyhJyspLeXPzq3iJHemPKhW4v3cqje4cvhUFuISD0NcHEAMbkT0HU7CgUV3MmfVtAPRefvrjE34IjmJHxIn10oLWcjQGXbVj0mhnugeFdZ3PM1Sznbrv51LOhoCmTT6VwNKsXSvFE6DWdBQd3uJsDAkgAD5CpW9huGoihWEsNz5nX86lc/ljtOHJEWrFWuVSuu9Gw/OtuSyQNfpWD2qXW23kyn0GUj6mtlfmf3/AIrF7UnW2vkxPxAH0NEoXgQU3lVnNvNIVwD4WgxmA1y8jHImtvt/lH2dBet3GZGuXBbIZULEKFzAwT4XkDy30pZwhAZSTCkwTrsdDtyjl0rVxeDB0YDUSrLqCORU7EVUXYpPtWHF/e7b8NzqwA0b1jX/AHdKxl9dfPbQVo8IxDYW8C0d2/hY8o5E+h+U132h4X3VwFR/LfVeg6r7j8qALhHFbmGfMhkH2kJ8LfofP8tKfsBxdLyBk22IO6noaRUsqULmJGkbe/3aVRw/FvZuZlMcmB2I6H96UsWU88bwouqDoGUeE8vfEmPnRxxVnh1vucM3e37gi5fXRnkTksz7K9WO3nzz8FihcQMu3Mc1PMHzrnE4fd1HiAjziZge8k1IUKnCze8VxvEIyqBNtVmcuU7idSTJJ1OpqvivEGtW0RrVq4DMQRlgAeyo2Iqq1xtEE7zsB+/3FLTDc+ZPzqoY0xWbB2AeT3pj+pl/KPhTLgjntAc150m4a/NhLZ3BY+8kfkB8K3uB4rwgcxoa83JHt4r1FmKtOsXUJW5Z2I3AmVYeasB7iOlb/CP4nMydzj7KYi2wgnKskf1IfC3yrJxTaE9QQfMEailtsPppW+G9Vy/kTuHTH9kcBiwb3DcQLT7my5JX0g+NP+4dBSHxDBvaYpeSD+JSCp9CNPjrXXiQyCVI2INdXsSX9sknrvPqP36V204zKxn3cNIka+n6UC6RWscNzUx5jb/FVXlkeNf9Q/f1ot1Wb3pgLyn4+vxPxqzCYw2nLAAg6Mp2YdD+vKurmG5r4gOm/wAKqRABmbXy/X9KMGbPZuBe6VUU6wd/PMSd6su8QS2MtsZm68hWEbJA9uBud5JPP/x0quxlGpYkA7eLxHpoZ+FFEu2doOZyfuqDr7hqfpVww7LplS35SC3wXT4mu7uIYkWlC2vxhABHWY1PTUnWrlwqjUsxHJdJb3xoPP8AYAUoWi2s67mqeKWwAscpH7+dbFmFzGACBoBsv5++s3ELKnSYagyVUnlVlq0RqaMCnzqm8SNxQ1UgkwK2MBaSwc1xgbnIaQn+fOsezdEz5fOig1TKb6axy8bvTaOPT8Q+NSscGva5/FHT57+N7id6w11/s3eCyDoX+91KTrk6TrQwYf4+NVW3/wA61ahA9DMzttz0/fzro4ukfXTU8v2PdWFx9v5sHZVA9Nz9CKYVMxyERpPyFLvFNbrnfWPgMv5VQ14DsdYuWM4S4+k5kugP7gVKe6PfSf8Aau5d7YzNaDey8Bh5iNFb00PMUbwVLg/4N17TEkDKxCnSdV2NZ9/C3GVrxgmTnGzK06yPnpRGtau2jbLE5rZ301B6Ecj+xVI42jYZrDoSQw7ozqFnST1G3vrM4ZjTaeYDKwKOjbOjCCD0PMEaggEUThcCjMwMgDYSPqJ1iNOs7kQaA3tkCeU/uaLXDwguP7J284008/lpWpgcCFIt35C3BmtmILprqB7j8NJBFYWLlTkklVOnp+tQHcN4sbVwfg0Vo3ZRsf7h/ivoCWgyh1IIMGRzUjQ/CvlczTt/DjjYVjhbuqsCbXru9v8A1CWHmvnVVm9rOE90/fKPA51/pff56n41hJrvX1HjGEV0e0dVYaHy3U+oMfCvmV20UYo3tKYNEeq5Gora4beg5hsd/KsGass4lkPUdKxnjt048/E3Yy74TyHWgjPPX961hcT4obihAIUanqT5+QoKzxB10kkdD09amGPjF5c/KmRxIMe+hblnpVGG4krCJ189/jz9KKe5POevWtuQQypkaelW2MeVIPsnqPzX9+lU3rkT+/rQrXxRTLYwmGxA/wCjc/Ha1tk/1WzEe7LWXxvs3iLQLsme3/1bfiWP6huvvHvrPtXIMqYPloaYeC9q7tk66jnH6bfSpoKbXCxAnyovCW8hzNuNp5Hr601cbwmHxatfw5Fq6qMWRFEXSBIBEjKxOkxrO1J+Huxy0NVGhhDJJCFuvKfWTtRfeGZjX1rNsSdTp6UaH60UQzQhndj9KFtjwnzrstPuqXDrA2igrUUNjUzH0owCqnSsx15L1pnQR7q7t3que3VDW604i1u+Y99Sgfj8DUoGi1h59jc65TtpvlJ+h0ruwcw9N+v7/SpUqKY+zPZd8WLji4LYRguqliTE7AjYEUiYtFC55nNqNOutSpVQ+fw57FDEIl25cKo1suAgGYEv4dWBHs+VB9vOB2sHigLDs4IQX7be0SwOVw0BeYEetSpT6Pso4fhSklhOXlJ/elbVzBJhLa3Lqg3Li5raHVVQyA7xoToYX49DKlWeilviOPe46uzFnkGT7o/LyAgULeUqYYR8DUqVBy1vmK4S4VIZTDKQQRyIMg/GpUoPpGD4n31pLnNhJHRgYYekg0r9p7ADi4PvCD6jn8PpUqUVi14Xj0qVKqOGUHb4UOwqVKDiKus4tl5yOhqVKiCbmLDDag3Y1KlFeZzV1rFkb6/WpUoDsPe2ZSQRsRIIqrEjWSN9dNPkNK9qUHuFeiJ11qVKKtDV2DUqUHSGvHWpUob2qZapdalSgpNqpUqUR//Z"/></div>
	<div><img src="/static/image/muscleman1.png" width="100%" height="40%"/> </div>	
  </div>
  <div id="article">
  		
  </div>
  <div id="aside_right">
  		<div><img src="https://www.primalsa.co.za/wordpress/wp-content/uploads/2019/10/Maximize-weight-training-feature-image-1200x600.jpg" width="100%" height="40%"/> </div>
  		<div><img src="https://content.active.com/Assets/Active.com+Content+Site+Digital+Assets/Article+Image+Update/Fitness/Benefits+of+Weight+Training/Carousel.jpg" width="100%" height="40%"/></div>
  		<div><img src="/static/image/muscleman1.png" width="100%" height="40%"/> </div>
  		<div><img src="https://content.active.com/Assets/Active.com+Content+Site+Digital+Assets/Article+Image+Update/Fitness/Benefits+of+Weight+Training/Carousel.jpg" width="100%" height="40%"/></div>
  		<div><img src="https://www.primalsa.co.za/wordpress/wp-content/uploads/2019/10/Maximize-weight-training-feature-image-1200x600.jpg" width="100%" height="40%"/> </div>
  		<div><img src="/static/image/muscleman1.png" width="100%" height="40%"/> </div>
  		<div><img src="https://www.primalsa.co.za/wordpress/wp-content/uploads/2019/10/Maximize-weight-training-feature-image-1200x600.jpg" width="100%" height="40%"/> </div>
  		<div><img src="https://content.active.com/Assets/Active.com+Content+Site+Digital+Assets/Article+Image+Update/Fitness/Benefits+of+Weight+Training/Carousel.jpg" width="100%" height="40%"/></div>
  </div>
</div>

<div id="footer">Footer</div>





</body>
</html>